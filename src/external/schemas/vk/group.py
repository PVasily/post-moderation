from typing import Optional

from pydantic import BaseModel, Field, AliasChoices


class Group(BaseModel):
    id: int = Field(..., description="Идентификатор сообщества.", validation_alias=AliasChoices("id", "group_id"))
    name: str = Field(..., description="Название сообщества.", validation_alias=AliasChoices("name"))
    screen_name: str = Field(..., description="Короткий адрес.", validation_alias=AliasChoices("screen_name"))
    is_closed: int = Field(..., description="Является ли сообщество закрытым.", validation_alias=AliasChoices("is_closed"))
    type: str = Field(..., description="Тип сообщества.", validation_alias=AliasChoices("type"))
    photo_50: Optional[str] = Field(None, description="URL главной фотографии с размером 50x50px.", validation_alias=AliasChoices("photo_50"))
    photo_100: Optional[str] = Field(None, description="URL главной фотографии с размером 100х100px.", validation_alias=AliasChoices("photo_100"))
    photo_200: Optional[str] = Field(None, description="URL главной фотографии в максимальном размере.", validation_alias=AliasChoices("photo_200"))
