from enum import Enum
from typing import List

from pydantic import BaseModel, Field, AliasChoices


class ChatType(str, Enum):
    CHAT = "CHAT"
    USER = "USER"


class Chat(BaseModel):
    peer_id: int = Field(
        ..., description="Идентификатор беседы.",
        validation_alias=AliasChoices("peerId", "peer_id")
    )

    type: ChatType = Field(
        ..., description="Тип чата.",
        validation_alias=AliasChoices("type")
    )

    title: str = Field(
        ..., description="Название чата.",
        validation_alias=AliasChoices("title")
    )

    photos: List[str] = Field(
        [], description="Список фотографий чата.",
        validation_alias=AliasChoices("photos")
    )