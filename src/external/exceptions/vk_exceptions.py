class VKException(Exception):
    type = "VK_EXCEPTION"

    @property
    def detail(self) -> str:
        return "Ошибка при запросе ВКонтакте."

    def __str__(self):
        return self.detail


def handle_error(code: int) -> None:
    if code == 1:
        raise UnknownException()
    if code == 2:
        raise ApplicationTurnedOff()
    if code == 3:
        raise UnknownMethod()
    if code == 4:
        raise IncorrectSignature()
    if code == 5:
        raise FailedAuthorization()
    if code == 6:
        raise TooManyRequests()
    if code == 7:
        raise HasNotPermissions()
    if code == 8:
        raise IncorrectRequest()
    if code == 9:
        raise TooManySameActions()
    if code == 10:
        raise InternalServerError()
    if code == 11:
        raise TestModeError()
    if code == 14:
        raise CaptchaRequired()
    if code == 15:
        raise AccessDenied()
    if code == 16:
        raise IncorrectProtocol()
    if code == 17:
        raise UserValidationRequired()
    if code == 18:
        raise PageWasDeleted()
    if code == 20:
        raise IncorrectStandaloneApplicationType()
    if code == 21:
        raise IncorrectOpenAPIApplicationType()
    if code == 23:
        raise MethodWasTurnedOff()
    if code == 24:
        raise UserConfirmationRequired()
    if code == 27:
        raise InvalidGroupAccessKey()
    if code == 28:
        raise InvalidApplicationAccessKey()
    if code == 29:
        raise MethodRequestsLimit()
    if code == 30:
        raise PrivateProfile()
    if code == 100:
        raise RequiredParamNotPassed()
    if code == 101:
        raise InvalidIdApplicationAPI()
    if code == 113:
        raise InvalidUserID()
    if code == 150:
        raise InvalidTimestamp()
    if code == 200:
        raise AlbumAccessDenied()
    if code == 201:
        raise AudioAccessDenied()
    if code == 203:
        raise GroupAccessDenied()
    if code == 300:
        raise AlbumIsFull()
    if code == 500:
        raise InvalidApplicationSettings()
    if code == 600:
        raise NoRightsAdvertisingCabinet()
    if code == 603:
        raise AdvertisingCabinetError()


class UnknownException(VKException):
    code = 1
    type = "UNKNOWN_EXCEPTION"

    @property
    def detail(self) -> str:
        return "Произошла неизвестная ошибка."


class ApplicationTurnedOff(VKException):
    code = 2
    type = "APPLICATION_TURNED_OFF"

    @property
    def detail(self) -> str:
        return "Приложение выключено."


class UnknownMethod(VKException):
    code = 3
    type = "UNKNOWN_METHOD"

    @property
    def detail(self) -> str:
        return "Передан неизвестный метод."


class IncorrectSignature(VKException):
    code = 4
    type = "INCORRECT_SIGNATURE"

    @property
    def detail(self) -> str:
        return "Неверная подпись."


class FailedAuthorization(VKException):
    code = 5
    type = "FAILED_AUTHORIZATION"

    @property
    def detail(self) -> str:
        return "Неудачная авторизация."


class TooManyRequests(VKException):
    code = 6
    type = "TOO_MANY_REQUESTS"

    @property
    def detail(self) -> str:
        return "Слишком много запросов."


class HasNotPermissions(VKException):
    code = 7
    type = "HAS_NOT_PERMISSIONS"

    @property
    def detail(self) -> str:
        return "Нет прав для выполнения этого действия."


class IncorrectRequest(VKException):
    code = 8
    type = "INCORRECT_REQUESTS"

    @property
    def detail(self) -> str:
        return "Неверный запрос"


class TooManySameActions(VKException):
    code = 9
    type = "TOO_MANY_SAME_ACTIONS"

    @property
    def detail(self) -> str:
        return "Слишком много однотипных действий."


class InternalServerError(VKException):
    code = 10
    type = "INTERNAL_SERVER_ERROR"

    @property
    def detail(self) -> str:
        return "Произошла внутренняя ошибка сервера."


class TestModeError(VKException):
    code = 11
    type = "TEST_MODE_ERROR"

    @property
    def detail(self) -> str:
        return "В тестовом режиме приложение должно быть выключено или пользователь должен быть залогинен."


class CaptchaRequired(VKException):
    code = 14
    type = "CAPTCHA_REQUIRED"

    @property
    def detail(self) -> str:
        return "Требуется ввод кода с картинки (Captcha)."


class AccessDenied(VKException):
    code = 15
    type = "ACCESS_DENIED"

    @property
    def detail(self) -> str:
        return "Доступ запрещён."


class IncorrectProtocol(VKException):
    code = 16
    type = "INCORRECT_PROTOCOL"

    @property
    def detail(self) -> str:
        return "Требуется выполнение запросов по протоколу HTTPS, т.к. пользователь включил настройку, требующую работу через безопасное соединение."


class UserValidationRequired(VKException):
    code = 17
    type = "USER_VALIDATION_REQUIRED"

    @property
    def detail(self) -> str:
        return "Требуется валидация пользователя"


class PageWasDeleted(VKException):
    code = 18
    type = "PAGE_WAS_DELETED"

    @property
    def detail(self) -> str:
        return "Страница удалена или заблокирована."


class IncorrectStandaloneApplicationType(VKException):
    code = 20
    type = "INCORRECT_APPLICATION_TYPE"

    @property
    def detail(self) -> str:
        return "Данное действие запрещено для не Standalone приложений."


class IncorrectOpenAPIApplicationType(VKException):
    code = 21
    type = "INCORRECT_APPLICATION_TYPE"

    @property
    def detail(self) -> str:
        return "Данное действие разрешено только для Standalone и Open API приложений."


class MethodWasTurnedOff(VKException):
    code = 23
    type = "METHOD_WAS_TURNED_OFF"

    @property
    def detail(self) -> str:
        return "Метод был выключен."


class UserConfirmationRequired(VKException):
    code = 24
    type = "USER_CONFIRMATION_REQUIRED"

    @property
    def detail(self) -> str:
        return "Требуется подтверждение со стороны пользователя"


class InvalidGroupAccessKey(VKException):
    code = 27
    type = "INVALID_GROUP_ACCESS_KEY"

    @property
    def detail(self) -> str:
        return "Ключ доступа сообщества недействителен."


class InvalidApplicationAccessKey(VKException):
    code = 28
    type = "INVALID_APPLICATION_ACCESS_KEY"

    @property
    def detail(self) -> str:
        return "Ключ доступа приложения недействителен."


class MethodRequestsLimit(VKException):
    code = 29
    type = "METHOD_REQUESTS_LIMIT"

    @property
    def detail(self) -> str:
        return "Достигнут количественный лимит на вызов метода"


class PrivateProfile(VKException):
    code = 30
    type = "PRIVATE_PROFILE"

    @property
    def detail(self) -> str:
        return "Профиль является приватным"


class RequiredParamNotPassed(VKException):
    code = 100
    type = "REQUIRED_PARAM_NOT_PASSED"

    @property
    def detail(self) -> str:
        return "Один из необходимых параметров был не передан или неверен."


class InvalidIdApplicationAPI(VKException):
    code = 101
    type = "INVALID_APPLICATION_API_ID"

    @property
    def detail(self) -> str:
        return "Неверный API ID приложения."


class InvalidUserID(VKException):
    code = 113
    type = "INVALID_USER_ID"

    @property
    def detail(self) -> str:
        return "Неверный идентификатор пользователя."


class InvalidTimestamp(VKException):
    code = 150
    type = "INVALID_TIMESTAMP"

    @property
    def detail(self) -> str:
        return "Неверный timestamp."


class AlbumAccessDenied(VKException):
    code = 200
    type = "ALBUM_ACCESS_DENIED"

    @property
    def detail(self) -> str:
        return "Доступ к альбому запрещён."


class AudioAccessDenied(VKException):
    code = 201
    type = "AUDIO_ACCESS_DENIED"

    @property
    def detail(self) -> str:
        return "Доступ к аудио запрещён."


class GroupAccessDenied(VKException):
    code = 203
    type = "GROUP_ACCESS_DENIED"

    @property
    def detail(self) -> str:
        return "Доступ к группе запрещён."


class AlbumIsFull(VKException):
    code = 300
    type = "ALBUM_IS_FULL"

    @property
    def detail(self) -> str:
        return "Альбом переполнен."


class InvalidApplicationSettings(VKException):
    code = 500
    type = "INVALID_APPLICATION_SETTINGS"

    @property
    def detail(self) -> str:
        return "Действие запрещено. Вы должны включить переводы голосов в настройках приложения."


class NoRightsAdvertisingCabinet(VKException):
    code = 600
    type = "NO_RIGHTS_ADVERTISING_CABINET"

    @property
    def detail(self) -> str:
        return "Нет прав на выполнение данных операций с рекламным кабинетом."


class AdvertisingCabinetError(VKException):
    code = 603
    type = "ADVERTISING_CABINET_ERROR"

    @property
    def detail(self) -> str:
        return "Произошла ошибка при работе с рекламным кабинетом."
