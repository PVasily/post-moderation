from random import randint
from typing import Union, List, Optional, Dict

import requests

from external.exceptions.vk_exceptions import handle_error
from external.schemas.vk.chat import Chat
from external.schemas.vk.group import Group


class VKClient:
    def __init__(self, session: Optional[requests.Session] = None):
        self.session = session

    def get_group_by_id(
            self, access_token: str, group_ids: Optional[List[Union[int, str]]] = None ,
            group_id: Optional[Union[int, str]] = None, fields: Optional[List[str]] = None
    ) -> List[Group]:
        """
        Возвращает информацию о заданном сообществе или о нескольких сообществах.

        Этот метод можно вызвать с ключом доступа пользователя, сообщества и с сервисным ключом доступа.

        Обязательно передать group_ids или group_id.

        :param access_token: Ключ доступа.
        :param group_ids: Идентификаторы или короткие имена сообществ. Максимальное число идентификаторов — 500.
        :param group_id: Идентификатор или короткое имя сообщества.
        :param fields: Список дополнительных полей, которые необходимо вернуть.

        :return: После успешного выполнения возвращает массив объектов, описывающих сообщества.
        """

        if group_id is None and group_ids is None:
            raise Exception("Одно из полей должно иметь значение!")

        request_params = {
            "access_token": access_token,
            "v": "5.199"
        }

        if group_ids is not None:
            request_params["group_ids"] = ",".join(map(str, group_ids))

        if group_id is not None:
            request_params["group_id"] = str(group_id)

        if fields is not None:
            request_params["fields"] = ",".join(fields)

        url = "https://api.vk.com/method/groups.getById"

        if self.session is not None:
            response = self.session.get(url, params=request_params)
        else:
            response = requests.get(url, params=request_params)

        json = response.json()

        response, error = json.get("response"), json.get("error")
        if response:
            return [Group.model_validate(group) for group in json["response"]["groups"]]
        handle_error(error["error_code"])

    def create_chat(
            self, access_token: str, title: str,
            user_ids: Optional[List[Union[int, str]]] = None, group_id: Optional[int] = None
    ) -> int:
        """
        Создаёт чат с несколькими участниками.

        Этот метод можно вызвать с ключом доступа пользователя или сообщества.

        :param access_token: Ключ доступа.
        :param title: Название чата.
        :param user_ids: Идентификаторы пользователей, которые будут участвовать в чате.
        :param group_id: Идентификатор сообщества (для сообщений сообщества с ключом доступа пользователя).

        :return: Возвращает идентификатор созданного чата (chat_id).
        """

        request_params = {
            "title": title,
            "access_token": access_token,
            "v": "5.199"
        }

        if user_ids is not None:
            request_params["user_ids"] = ",".join(map(str, user_ids))

        if group_id is not None:
            request_params["group_id"] = str(group_id)

        url = "https://api.vk.com/method/messages.createChat"

        if self.session is not None:
            response = self.session.get(url, params=request_params)
        else:
            response = requests.get(url, params=request_params)

        json = response.json()

        response, error = json.get("response"), json.get("error")
        if response:
            return response["chat_id"]
        handle_error(error["error_code"])

    def get_invite_link(self, access_token: str, peer_id: int, reset: int = 0) -> str:
        """
        Получает ссылку для приглашения пользователя в беседу.

        :param access_token: Ключ-доступа.
        :param peer_id: Идентификатор назначения.
        :param reset: 1 — сгенерировать новую ссылку, сбросив предыдущую. 0 — получить предыдущую ссылку.

        :return: Возвращает ссылку для приглашения в беседу.
        """

        request_params = {
            "peer_id": peer_id,
            "reset": reset,
            "access_token": access_token,
            "v": "5.199"
        }

        url = "https://api.vk.com/method/messages.getInviteLink"

        if self.session is not None:
            response = self.session.get(url, params=request_params)
        else:
            response = requests.get(url, params=request_params)

        json = response.json()

        response, error = json.get("response"), json.get("error")
        if response:
            return response["link"]
        handle_error(error["error_code"])

    def get_callback_confirmation_code(self, access_token: str, group_id: int) -> str:

        request_params = {
            "group_id": str(group_id),
            "access_token": access_token,
            "v": "5.199"
        }

        url = "https://api.vk.com/method/groups.getCallbackConfirmationCode"

        if self.session is not None:
            response = self.session.get(url, params=request_params)
        else:
            response = requests.get(url, params=request_params)

        json = response.json()

        response, error = json.get("response"), json.get("error")
        if response:
            return response["code"]
        handle_error(error["error_code"])

    def send_message_to_chat(self, access_token: str, peer_id: Optional[int] = None, peer_ids: Optional[List[int]] = None, message: Optional[str] = None, attachment: Optional[str] = None):
        request_params = {
            "random_id": randint(1, 100000),
            "access_token": access_token,
            "peer_id": peer_id,
            "v": "5.199"
        }

        if message:
            request_params["message"] = message

        if peer_id:
            request_params["peer_id"] = str(peer_id)

        if peer_ids:
            request_params["peer_ids"] = ",".join(map(str, peer_ids))

        if attachment:
            request_params["attachment"] = attachment

        url = "https://api.vk.com/method/messages.send"

        if self.session is not None:
            response = self.session.get(url, params=request_params)
        else:
            response = requests.get(url, params=request_params)

        json = response.json()

        response, error = json.get("response"), json.get("error")
        if response or error is None:
            return
        handle_error(error["error_code"])

    def wall_attachment(self, group_id: int, post_id: int) -> str:
        return f"wall-{group_id}_{post_id}"

    def get_chats(self, access_token: str) -> Dict[int, Chat]:
        """Получить список чатов."""

        request_params = {
            "access_token": access_token,
            "v": "5.199"
        }
        url = "https://api.vk.com/method/messages.getConversations"

        response = requests.get(url, params=request_params)

        json = response.json()
        response, error = json.get("response"), json.get("error")
        if response or error is None:
            items = response["items"]
            chats = dict()

            for item in items:
                type = item["conversation"]["peer"]["type"]
                if type != "chat":
                    continue
                peer_id = item["conversation"]["peer"]["id"]
                title = item["conversation"]["chat_settings"]["title"]

                photos = [
                    item["conversation"]["chat_settings"]["photo"]["photo_50"],
                    item["conversation"]["chat_settings"]["photo"]["photo_100"],
                    item["conversation"]["chat_settings"]["photo"]["photo_200"]
                ]

                chat = Chat(title=title, peer_id=peer_id, photos=photos, type="CHAT")
                chats[chat.peer_id] = chat
            return chats
        handle_error(error["error_code"])
