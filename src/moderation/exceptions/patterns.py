class PatternNotFound(Exception):
    type = "PATTERN_NOT_FOUND"

    def __init__(self, pattern_id: int) -> None:
        self.pattern_id = pattern_id

    @property
    def detail(self) -> str:
        return "Не удалось найти паттерн."
