class ChatNotFound(Exception):
    type = "CHAT_NOT_FOUND"

    def __init__(self, group_id: int, peer_id: int) -> None:
        self.group_id = group_id
        self.peer_id = peer_id

    @property
    def detail(self) -> str:
        return "Не удалось найти чат."
