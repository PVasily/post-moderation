from typing import Union


class GroupNotFound(Exception):
    type = "GROUP_NOT_FOUND"

    def __init__(self, group_ident: Union[int, str]) -> None:
        self.group_ident = group_ident

    @property
    def detail(self) -> str:
        return "Не удалось найти сообщество."
