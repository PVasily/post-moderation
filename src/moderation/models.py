from django.db import models


class Group(models.Model):
    group_id = models.IntegerField(verbose_name="Уникальный идентификатор сообщества.", primary_key=True)
    access_token = models.CharField(max_length=300, verbose_name="Ключ-доступа сообщества.")
    is_disable = models.BooleanField(verbose_name="Сообщество неактивно?", default=False)
    created_at = models.DateTimeField(verbose_name="Дата-время создания записи.", auto_now_add=True)

    def __str__(self) -> str:
        return str(self.group_id)


class Chat(models.Model):
    group_id = models.IntegerField(verbose_name="Идентификатор сообщества.")
    peer_id = models.IntegerField(verbose_name="Идентификатор чата")
    created_at = models.DateTimeField(verbose_name="Дата-время создания записи.", auto_now_add=True)

    def __str__(self):
        return f"{self.group_id} - {self.peer_id}"

    @staticmethod
    def has_chat(group_id: int, peer_id: int) -> bool:
        """Есть ли такой чат?"""

        if Chat.objects.filter(group_id=group_id, peer_id=peer_id).count():
            return True
        return False


class Pattern(models.Model):
    pattern = models.TextField(verbose_name="Паттерн.")
    group_id = models.IntegerField(verbose_name="Идентификатор сообщества.")
    created_at = models.DateTimeField(verbose_name="Дата-время создания записи.", auto_now_add=True)

    def __str__(self):
        return str(self.pattern)
