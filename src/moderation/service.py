from typing import Optional, Union, List

from django.core.exceptions import ObjectDoesNotExist

from config import settings
from external.clients.vk import VKClient
from moderation.dto.group import GroupDTO
from moderation.models import Group


def get_group(group: Union[int, str]) -> Optional[GroupDTO]:
    """
    Получить группу.

    :param group: Уникальный идентификатор или короткий адрес сообщества.
    """

    # Получаем информацию о сообществе по уникальному идентификатору или короткому адресу.
    vk_client = VKClient()
    group_information, *_ = vk_client.get_group_by_id(group_id=group, access_token=settings.VK_APPLICATION_TOKEN)

    # Проверяем, что сообщество добавлено в базу данных.
    try:
        group = Group.objects.get(group_id=group_information.id)
    except ObjectDoesNotExist:
        return

    # Собираем всю информацию воедино.
    photos = [photo for photo in [group_information.photo_50, group_information.photo_100, group_information.photo_200] if photo]
    return GroupDTO(
        id=group_information.id, access_token=group.access_token, name=group_information.name,
        screen_name=group_information.screen_name, is_closed=group_information.is_closed,
        type=group_information.type, photos=photos, url=f"https://vk.com/{group_information.screen_name}"
    )


def get_groups(group_ids: List[int]) -> List[GroupDTO]:
    result = []

    if not group_ids:
        return result

    vk_client = VKClient()
    groups_information = vk_client.get_group_by_id(group_ids=group_ids, access_token=settings.VK_APPLICATION_TOKEN)
    for group_information in groups_information:
        group = Group.objects.get(group_id=group_information.id)
        photos = [photo for photo in [group_information.photo_50, group_information.photo_100, group_information.photo_200] if photo]
        result.append(
            GroupDTO(
                id=group_information.id, access_token=group.access_token, name=group_information.name,
                screen_name=group_information.screen_name, is_closed=group_information.is_closed,
                type=group_information.type, photos=photos, url=f"https://vk.com/{group_information.screen_name}"
            )
        )
    return result


def add_group(group: Union[int, str], access_token: str) -> GroupDTO:
    """
    Добавить сообщество.

    :param group: Уникальный идентификатор или короткий адрес сообщества.
    :param access_token: Ключ доступа сообщества.
    """

    vk_client = VKClient()
    group_information, *_ = vk_client.get_group_by_id(group_id=group, access_token=access_token)
    group = Group.objects.create(group_id=group_information.id, access_token=access_token)

    # Собираем всю информацию воедино.
    photos = [photo for photo in [group_information.photo_50, group_information.photo_100, group_information.photo_200] if photo]
    return GroupDTO(
        id=group_information.id, access_token=group.access_token, name=group_information.name,
        screen_name=group_information.screen_name, is_closed=group_information.is_closed,
        type=group_information.type, photos=photos, url=f"https://vk.com/{group_information.screen_name}"
    )


def get_group_by_ident(group_ident: str):
    is_url = group_ident[::-1].find("/")

    if is_url >= 0:
        return group_ident[len(group_ident) - is_url:]
    return group_ident
