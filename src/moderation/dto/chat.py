from typing import List

from pydantic import AliasChoices, BaseModel, Field


class ChatDTO(BaseModel):
    group_id: int = Field(
        ..., description="Идентификатор сообщества.",
        validation_alias=AliasChoices("groupId", "group_id")
    )

    peer_id: int = Field(
        ..., description="Идентификатор чата.",
        validation_alias=AliasChoices("peerId", "peer_id")
    )

    title: str = Field(
        ..., description="Название чата.",
        validation_alias=AliasChoices("title")
    )

    photos: List[str] = Field(
        [], description="Список фотографий чата.",
        validation_alias=AliasChoices("photos")
    )
