from pydantic import BaseModel, Field


class PatternDTO(BaseModel):
    pattern_id: int = Field(..., description="Идентификатор паттерна.")
    group_id: int = Field(..., description="Идентификатор сообщества.")
    pattern: str = Field(..., description="Паттерн.")
