from typing import List

from pydantic import BaseModel, Field


class GroupDTO(BaseModel):
    id: int = Field(..., description="Уникальный идентификатор сообщества.")
    access_token: str = Field(..., description="Ключ доступа сообщества.")
    name: str = Field(..., description="Название сообщества.")
    screen_name: str = Field(..., description="Короткий адрес сообщества.")
    url: str = Field(..., description="URL сообщества.")
    is_disable: int = Field(False, description="Сообщество неактивно?")
    type: str = Field(..., description="Тип сообщества.")
    photos: List[str] = Field([], description="Список URL главной фотографии сообщества.")
