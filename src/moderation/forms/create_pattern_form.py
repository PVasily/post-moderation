from django import forms


class CreatePatternForm(forms.Form):
    pattern = forms.CharField(label="Сообщество")

    def clean_pattern(self) -> str:
        pattern = self.cleaned_data["pattern"]
        return pattern.lower()
