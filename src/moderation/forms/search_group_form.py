from django import forms


class SearchGroupForm(forms.Form):
    group_ident = forms.CharField(label="Сообщество")
    include_disabled = forms.BooleanField(label="Добавить отключенные сообщества.", required=False)

    def clean_group_ident(self) -> str:
        group_ident = self.cleaned_data["group_ident"]
        is_url = group_ident[::-1].find("/")

        if is_url >= 0:
            return group_ident[len(group_ident) - is_url:]
        return group_ident
