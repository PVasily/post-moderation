import json

from django.http import HttpRequest, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from jarowinkler import jarowinkler_similarity

from moderation.services import groups, patterns


@require_POST
@csrf_exempt
def callback(request: HttpRequest):
    callback_request = json.loads(request.body.decode("utf-8"))

    event_type = callback_request.get("type")
    group_id = callback_request.get("group_id")
    event = callback_request.get("object")

    result = HttpResponse("ok")
    result.status_code = 200

    if event_type == "confirmation":
        confirmation_code = groups.get_confirmation_code(group_id)
        return HttpResponse(confirmation_code, status=200)
    elif event_type == "wall_post_new":
        text: str = event.get("text", "")
        text = text.replace("\n", " ")
        patterns_list = [pattern.pattern.lower() for pattern in patterns.list(group_id)]

        characters = [".", ",", "?", "!", ":", ";", "\n", "\t"]
        for pattern in patterns_list:
            flag = False
            for word in [word.lower() for word in text.split(" ")]:
                word = word.lower()
                for char in characters:
                    word = word.replace(char, "")
                word = word.strip()

                if pattern.lower() == word: #jarowinkler_similarity(pattern, word, score_cutoff=0.9) > 0:
                    flag = True
                    groups.reply_post(group_id, event["id"])
                if flag:
                    break
            if flag:
                break
        else:
            print(event)

    return result
