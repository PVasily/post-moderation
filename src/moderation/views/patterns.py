from django.contrib.auth.decorators import login_required
from django.http import HttpRequest
from django.shortcuts import redirect

from moderation.forms.create_pattern_form import CreatePatternForm
from moderation.services import patterns


# @login_required(login_url="login")
def create(request: HttpRequest, group_id: int):
    if request.user.is_anonymous:
        return redirect("/login")

    """
    Создать паттерн.

    :param request: HTTP запрос.
    :param group_id: Идентификатор сообщества.
    """
    if request.method == "POST":
        form = CreatePatternForm(request.POST)
        if form.is_valid():
            patterns.create(group_id, form.cleaned_data["pattern"])
    return redirect(request.META.get("HTTP_REFERER", "/"))


# @login_required(login_url="login")
def delete(request: HttpRequest, group_id: int, pattern_id: int):
    if request.user.is_anonymous:
        return redirect("/login")

    """
    Удалить паттерн.

    :param request: HTTP запрос.
    :param group_id: Идентификатор сообщества.
    :param pattern_id: Идентификатор паттерна.
    """

    patterns.delete(pattern_id)

    return redirect(request.META.get("HTTP_REFERER", "/"))
