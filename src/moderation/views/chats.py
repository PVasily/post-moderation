from django.contrib.auth.decorators import login_required
from django.http import HttpRequest, HttpResponse
from django.shortcuts import redirect

from external.exceptions.vk_exceptions import VKException
from moderation.exceptions.chats import ChatNotFound
from moderation.exceptions.groups import GroupNotFound
from moderation.forms.create_chat_form import CreateChatForm
from moderation.services import chats


# @login_required(login_url="login")
def create(request: HttpRequest, group_id: int) -> HttpResponse:
    if request.user.is_anonymous:
        return redirect("/login")

    if request.method == "POST":
        form = CreateChatForm(request.POST)
        if form.is_valid():
            try:
                chats.create(group_id,  title=form.cleaned_data["title"])
            except GroupNotFound or VKException:
                pass
    return redirect(request.META.get("HTTP_REFERER", "/"))


# @login_required(login_url="login")
def delete(request: HttpRequest, group_id: int, chat_id: int) -> HttpResponse:
    if request.user.is_anonymous:
        return redirect("/login")

    try:
        chats.delete(group_id, chat_id)
    except ChatNotFound:
        pass
    return redirect(request.META.get("HTTP_REFERER", "/"))


# @login_required(login_url="login")
def reset_invite_link(request: HttpRequest, group_id: int, chat_id: int) -> HttpResponse:
    if request.user.is_anonymous:
        return redirect("/login")

    try:
        chats.reset_invite_link(group_id, chat_id)
    except ChatNotFound or GroupNotFound:
        pass
    return redirect(request.META.get("HTTP_REFERER", "/"))


# @login_required(login_url="login")
def disable(request: HttpRequest, group_id: int, chat_id: int) -> HttpResponse:
    if request.user.is_anonymous:
        return redirect("/login")

    try:
        chats.disable(group_id, chat_id)
    except ChatNotFound:
        pass
    return redirect(request.META.get("HTTP_REFERER", "/"))


# @login_required(login_url="login")
def activate(request: HttpRequest, group_id: int, chat_id: int) -> HttpResponse:
    if request.user.is_anonymous:
        return redirect("/login")

    try:
        chats.activate(group_id, chat_id)
    except ChatNotFound:
        pass
    return redirect(request.META.get("HTTP_REFERER", "/"))

