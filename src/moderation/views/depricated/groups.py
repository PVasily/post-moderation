from typing import Union

from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect

from moderation.forms.add_group_form import AddGroupForm
from moderation.services import groups


# @login_required(login_url="login")
def list(request: HttpRequest) -> HttpResponse:
    if request.user.is_anonymous:
        return redirect("/login")

    # TODO: Добавить сортировку результата
    # TODO: Рефакторинг ошибок API ВКонтакте
    # TODO: Рефакторинг поиска сообществ
    # TODO: Добавить вывод отключенных/включенных сообществ

    context = {
        "errors": []
    }

    if request.POST:
        form = AddGroupForm(request.POST)
        if form.is_valid():
            try:

                groups.create(form.cleaned_data["group_ident"], form.cleaned_data["access_token"])
                return HttpResponseRedirect("/")
            except Exception:
                context["errors"].append("ADD_GROUP_ERROR")

    # Получаем список сообществ
    #search = request.GET.get("search")
    #disabled = False if request.GET.get("disabled") == "False" else True

    groups_list = groups.list()

    # Пагинация
    paginator = Paginator(groups_list, 15)
    page = paginator.get_page(request.GET.get("page"))

    context["groups"] = page.object_list
    context["page_obj"] = page
    context["paginator"] = paginator

    return render(
        request, "templates/depricated/groups/list.html", context=context, status=200
    )


# @login_required(login_url="login")
def detail(request: HttpRequest, group_ident: Union[int, str]) -> HttpResponse:
    if request.user.is_anonymous:
        return redirect("/login")

    """
    Получить детальную информацию о сообществе.

    :param request: HTTP-запрос.
    :param group_ident: Уникальный идентификатор или короткое имя сообщества.
    """

    context = {}

    if group := groups.get(group_ident):
        context["group"] = group
        #context["chats"] = chats.list(group.id)
        #context["patterns"] = patterns.list(group.id)

        return render(request, "templates/depricated/groups/detail.html", context=context, status=200)
    return render(request, "templates/depricated/groups/not_found.html", status=404)
