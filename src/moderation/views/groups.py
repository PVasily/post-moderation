from django.contrib.auth.decorators import login_required
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render, redirect

from moderation.exceptions.groups import GroupNotFound
from moderation.forms.update_group_form import UpdateGroupForm
from moderation.services import groups, patterns, chats


# @login_required(login_url="login")
def detail(request: HttpRequest, group_ident: str) -> HttpResponse:
    if request.user.is_anonymous:
        return redirect("/login")

    """
    Получить детальное представление сообщества.

    GET: Возвращает сообщество.
    POST: Обновляет информацию о сообществе.

    :param request: HTTP-запрос.
    :param group_ident: Уникальный идентификатор или короткий адрес сообщества.
    """

    context = dict()
    context["chats"] = {"active": [], "inactive": []}

    # Обновить информацию о сообществе
    if request.method == "POST":
        form = UpdateGroupForm(request.POST)
        if form.is_valid():
            groups.update_token(group_ident, form.cleaned_data["access_token"])
            return redirect(request.META.get("HTTP_REFERER", "/"))
    try:
        group = groups.get(group_ident)

        context["group"] = group
        context["chats"]["active"], context["chats"]["inactive"] = chats.list(group.id)
        context["patterns"] = patterns.list(group.id)
    except GroupNotFound:
        return render(request, "templates/depricated/groups/not_found.html")
    return render(request, "templates/depricated/groups/detail.html", context=context)


# @login_required(login_url="login")
def delete(request: HttpRequest, group_id: int) -> HttpResponse:
    if request.user.is_anonymous:
        return redirect("/login")

    """
    Удалить сообщество.

    POST: Удаляет сообщество.

    :param request: HTTP-запрос.
    :param group_id: Идентификатор сообщества.
    """

    groups.delete(group_id)
    return redirect("/")


# @login_required(login_url="login")
def disable(request: HttpRequest, group_id: int) -> HttpResponse:
    if request.user.is_anonymous:
        return redirect("/login")

    """
    Отключить сообщество.

    POST: Отключает сообщество.

    :param request: HTTP-запрос.
    :param group_id: Идентификатор сообщества.
    """

    groups.disable(group_id)
    return redirect(request.META.get("HTTP_REFERER", "/"))


# @login_required(login_url="login")
def activate(request: HttpRequest, group_id: int) -> HttpResponse:
    if request.user.is_anonymous:
        return redirect("/login")

    """
    Активировать сообщество.

    GET: Активирует сообщество.

    :param request: HTTP-запрос.
    :param group_id: Идентификатор сообщества.
    """

    groups.activate(group_id)
    return redirect(request.META.get("HTTP_REFERER", "/"))
