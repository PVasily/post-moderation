from typing import List

from django.core.exceptions import ObjectDoesNotExist

from moderation.dto.pattern import PatternDTO
from moderation.exceptions.patterns import PatternNotFound
from moderation.models import Pattern


def list(group_id: int) -> List[PatternDTO]:
    """
    Получить список паттернов.

    :param group_id: Идентификатор сообщества.
    """

    patterns = Pattern.objects.filter(group_id=group_id).all()
    return [PatternDTO(pattern_id=pattern.id, group_id=group_id, pattern=pattern.pattern) for pattern in patterns]


def create(group_id: int, pattern: str) -> PatternDTO:
    """
    Добавить паттерн.

    :param group_id: Идентификатор сообщества.
    :param pattern: Паттерн.
    """

    pattern = Pattern.objects.create(group_id=group_id, pattern=pattern)
    return PatternDTO(pattern_id=pattern.id, group_id=pattern.group_id, pattern=pattern.pattern)


def delete(pattern_id: int) -> None:
    """
    Удалить паттерн.

    :param pattern_id: Идентификатор паттерна.

    :raise PatternNotFound: Не удалось найти паттерн.
    """

    try:
        pattern = Pattern.objects.get(id=pattern_id)
        pattern.delete()
    except ObjectDoesNotExist:
        raise PatternNotFound(pattern_id)
