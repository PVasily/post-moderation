from typing import List, Union, Optional

from django.core.exceptions import ObjectDoesNotExist
from jarowinkler import jarowinkler_similarity

from config import settings
from external.clients.vk import VKClient
from moderation.dto.group import GroupDTO
from moderation.exceptions.groups import GroupNotFound
from moderation.models import Group
from moderation.services import chats

vk_client = VKClient()


def get(group_ident: Union[int, str]) -> GroupDTO:
    """
    Получить сообщество.

    :param group_ident: Идентификатор, ссылка или короткий адрес сообщества.

    :raise GroupNotFound: Не удалось найти сообщество.
    """

    group_ident = validate_group_ident(group_ident)
    vk_group, *_ = vk_client.get_group_by_id(settings.VK_APPLICATION_TOKEN, group_id=group_ident)
    if vk_group is None:
        raise GroupNotFound(group_ident)

    try:
        group = Group.objects.get(group_id=vk_group.id)
    except ObjectDoesNotExist:
        raise GroupNotFound(vk_group.id)

    return GroupDTO(
        id=vk_group.id, access_token=group.access_token, name=vk_group.name, screen_name=vk_group.screen_name,
        url=f"https://vk.com/{vk_group.screen_name}", is_disable=group.is_disable, type=vk_group.type,
        photos=[photo for photo in [vk_group.photo_50, vk_group.photo_100, vk_group.photo_200] if photo]
    )


def update_token(group_ident: Union[int, str], access_token: str) -> None:
    group = get(group_ident)

    group = Group.objects.get(group_id=group.id)
    group.access_token = access_token
    group.save()


def list(group_ident: Optional[Union[int, str]] = None, include_disabled: bool = True) -> List[GroupDTO]:
    """
    Получить список сообществ.

    :param group_ident: Идентификатор, ссылка или короткий адрес сообщества.
    :param include_disabled: Включить отключенные сообщества?
    """

    group_ident = validate_group_ident(group_ident) if group_ident else None

    groups = Group.objects.all()
    if not include_disabled:
        groups = groups.exclude(is_disable=True)
    groups = groups.order_by("created_at")

    group_ids = [group.group_id for group in groups]
    if not group_ids:
        return []

    vk_groups = vk_client.get_group_by_id(settings.VK_APPLICATION_TOKEN, group_ids=group_ids)

    # Костыль.
    # ВКонтакте возвращает данные сообщества со своей сортировкой,
    # а нам нужно сортировать по дате добавления в базу данных.
    # Поэтому заносим всё в словарь, чтобы быстро получать данные сообщества
    vk_groups = {vk_group.id: vk_group for vk_group in vk_groups}

    result = []
    for group in groups:
        vk_group = vk_groups[group.group_id]
        group_dto = GroupDTO(
            id=vk_group.id, access_token=group.access_token, name=vk_group.name, screen_name=vk_group.screen_name,
            url=f"https://vk.com/{vk_group.screen_name}", is_disable=group.is_disable, type=vk_group.type,
            photos=[photo for photo in [vk_group.photo_50, vk_group.photo_100, vk_group.photo_200] if photo]
        )

        # Фильтруем сообщества
        if group_ident:
            # Проводим нечеткое сравнение строк
            id_match = group_ident == str(group_dto.id)
            name_match = jarowinkler_similarity(group_ident, group_dto.name, score_cutoff=0.65)
            screen_name_match = jarowinkler_similarity(group_ident, group_dto.screen_name, score_cutoff=0.65)

            if id_match or name_match or screen_name_match:
                result.append(group_dto)
        else:
            result.append(group_dto)

    return result


def create(group_ident: Union[int, str], access_token: str) -> GroupDTO:
    """
    Создать сообщество.

    :param group_ident: Уникальный идентификатор или короткий адрес сообщества.
    :param access_token: Ключ доступа сообщества.
    """

    group_ident = validate_group_ident(group_ident)
    vk_group, *_ = vk_client.get_group_by_id(group_id=group_ident, access_token=access_token)

    group = Group.objects.create(group_id=vk_group.id, access_token=access_token)

    return GroupDTO(
        id=vk_group.id, access_token=group.access_token, name=vk_group.name, screen_name=vk_group.screen_name,
        is_disable=group.is_disable, type=vk_group.type, url=f"https://vk.com/{vk_group.screen_name}",
        photos=[photo for photo in [vk_group.photo_50, vk_group.photo_100, vk_group.photo_200] if photo]
    )


def delete(group_id: int) -> None:
    """
    Удалить сообщество.

    :param group_id: Идентификатор сообщества.

    :raise GroupNotFound: Не удалось найти сообщество.
    """

    try:
        group = Group.objects.get(group_id=group_id)
        group.delete()
    except ObjectDoesNotExist:
        raise GroupNotFound(group_id)


def disable(group_id: int) -> None:
    """
    Отключить сообщество.

    :param group_id: Идентификатор сообщества.

    :raise GroupNotFound: Не удалось найти сообщество.
    """

    try:
        group = Group.objects.get(group_id=group_id)
        group.is_disable = True
        group.save()
    except ObjectDoesNotExist:
        raise GroupNotFound(group_id)


def activate(group_id: int) -> None:
    """
    Включить сообщество.

    :param group_id: Идентификатор сообщества.

    :raise GroupNotFound: Не удалось найти сообщество.
    """

    try:
        group = Group.objects.get(group_id=group_id)
        group.is_disable = False
        group.save()
    except ObjectDoesNotExist:
        raise GroupNotFound(group_id)


def validate_group_ident(group_ident: Union[int, str]) -> str:
    """
    Валидация сообщества.

    :param group_ident: Идентификатор, ссылка или короткий адрес сообщества.
    """

    group_ident = str(group_ident)
    is_url = group_ident[::-1].find("/")

    if is_url >= 0:
        return group_ident[len(group_ident) - is_url:]
    return group_ident


def reply_post(group_id: int, post_id: int):
    group = get(group_id)
    chats_active, chats_inactive = chats.list(group_id)

    peer_ids = [chat.peer_id for chat in chats_active]

    if peer_ids:
        attachment = vk_client.wall_attachment(group_id, post_id)
        vk_client.send_message_to_chat(group.access_token, peer_ids=peer_ids, attachment=attachment)


def get_confirmation_code(group_id: int) -> str:
    group = get(group_id)
    return vk_client.get_callback_confirmation_code(group.access_token, group.id)
