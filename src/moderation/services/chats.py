from typing import List, Optional, Tuple

from django.core.exceptions import ObjectDoesNotExist

from external.clients.vk import VKClient
from moderation.dto.chat import ChatDTO
from moderation.exceptions.chats import ChatNotFound
from moderation.models import Chat, Group
from moderation.services import groups


vk_client = VKClient()


def get(group_id: int, peer_id: int) -> ChatDTO:
    """
    Получить чат.

    :param group_id: Идентификатор сообщества.
    :param peer_id: Идентификатор чата.

    :raise ChatNotFound: Не удалось найти чат.
    """

    try:
        chat = Chat.objects.get(group_id=group_id, peer_id=peer_id)
    except ObjectDoesNotExist:
        raise ChatNotFound(group_id, peer_id)

    return ChatDTO(
        chat_id=chat.chat_id, group_id=chat.group_id, peer_id=2000000000 + chat.chat_id,
        is_disable=chat.is_disable, invite_link=chat.invite_link, title=chat.title
    )


def list(group_id: int) -> Tuple[List[ChatDTO], List[ChatDTO]]:
    """
    Получить список чатов для сообщества.

    :param group_id: Идентификатор сообщества.

    :return: Список активных чатов и список неактивных чатов
    """

    group = groups.get(group_id)

    active_chats = []
    inactive_chats = []

    # Получаем список чатов
    vk_chats = vk_client.get_chats(group.access_token)

    for chat in vk_chats.values():
        result = ChatDTO(group_id=group_id, peer_id=chat.peer_id, title=chat.title, photos=chat.photos)

        if Chat.has_chat(group_id, chat.peer_id):
            active_chats.append(result)
        else:
            inactive_chats.append(result)

    return active_chats, inactive_chats


def create(group_id: int, title: Optional[str] = None) -> ChatDTO:
    """
    Создать чат.

    Если не было передано название чата, то оно будет взято из названия сообщества.

    :param group_id: Идентификатор группы.
    :param title: Название чата.
    """

    group = groups.get(group_id)

    title = title if title else group.name

    chat_id = vk_client.create_chat(group.access_token, title)
    invite_link = vk_client.get_invite_link(group.access_token, 2000000000 + chat_id)

    chat = Chat.objects.create(title=title, chat_id=chat_id, group_id=group_id, invite_link=invite_link)
    chat.save()

    return ChatDTO(
        chat_id=chat.chat_id, group_id=group_id, peer_id=2000000000 + chat.chat_id,
        is_disable=chat.is_disable, invite_link=chat.invite_link, title=chat.title
    )


def delete(group_id: int, chat_id: int) -> None:
    """
    Удалить чат.

    :param group_id: Идентификатор сообщества.
    :param chat_id: Идентификатор чата.
    """

    try:
        chat = Chat.objects.get(group_id=group_id, chat_id=chat_id)
        chat.delete()
    except ObjectDoesNotExist:
        raise ChatNotFound(group_id, chat_id)


def reset_invite_link(group_id: int, chat_id: int) -> str:
    """
    Обновить ссылку-приглашение в чат.

    :param group_id: Идентификатор сообщества.
    :param chat_id: Идентификатор чата.

    :raise GroupNotFound: Не удалось найти сообщество.
    :raise ChatNotFound: Не удалось найти чат.
    """

    group = groups.get(group_id)

    try:
        chat = Chat.objects.get(group_id=group_id, chat_id=chat_id)
    except ObjectDoesNotExist:
        raise ChatNotFound(group_id, chat_id)

    invite_link = vk_client.get_invite_link(group.access_token, 2000000000 + chat.chat_id, reset=1)

    chat.invite_link = invite_link
    chat.save()

    return invite_link


def disable(group_id: int, peer_id: int) -> None:
    """
    Отключить чат.

    :param group_id: Идентификатор сообщества.
    :param peer_id: Идентификатор чата.

    :raise ChatNotFound: Не удалось найти чат.
    """

    try:
        chat = Chat.objects.get(group_id=group_id, peer_id=peer_id)
        chat.delete()
    except ObjectDoesNotExist:
        raise ChatNotFound(group_id, peer_id)


def activate(group_id: int, peer_id: int) -> None:
    """
    Включить чат.

    :param group_id: Идентификатор сообщества.
    :param peer_id: Идентификатор чата.

    :raise ChatNotFound: Не удалось найти чат.
    """

    Chat(group_id=group_id, peer_id=peer_id).save()

