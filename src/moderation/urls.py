from django.urls import path

from .views import chats, groups, patterns, callback
from .views.depricated.groups import list

urlpatterns = [
    # Сообщества
    path("", list, name="groups-list"),
    path("<str:group_ident>/", groups.detail, name="groups-detail"),
    path("groups/<int:group_id>/disable", groups.disable, name="groups-disable"),
    path("groups/<int:group_id>/activate", groups.activate, name="groups-activate"),
    path("groups/<int:group_id>/delete", groups.delete, name="groups-delete"),

    # Чаты
    path("groups/<int:group_id>/chats/create", chats.create, name="chats-create"),
    path("groups/<int:group_id>/chats/<int:chat_id>/delete", chats.delete, name="chats-delete"),
    path("groups/<int:group_id>/chats/<int:chat_id>/invites/reset", chats.reset_invite_link, name="chats-reset-invite-link"),
    path("groups/<int:group_id>/chats/<int:chat_id>/disable", chats.disable, name="chats-disable"),
    path("groups/<int:group_id>/chats/<int:chat_id>/activate", chats.activate, name="chats-activate"),

    # Паттерны
    path("groups/<int:group_id>/patterns/create", patterns.create, name="patterns-create"),
    path("groups/<int:group_id>/patterns/<int:pattern_id>/delete", patterns.delete, name="patterns-delete"),

    # Callback
    path("api/callback", callback.callback, name="api-callback")
]
