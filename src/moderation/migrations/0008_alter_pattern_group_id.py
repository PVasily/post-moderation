# Generated by Django 5.0.2 on 2024-02-24 01:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('moderation', '0007_alter_chat_title'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pattern',
            name='group_id',
            field=models.IntegerField(verbose_name='Идентификатор сообщества.'),
        ),
    ]
