from getpass import getpass

from django.core.management import BaseCommand
from django.db import IntegrityError

from authenticator.models import User


class Command(BaseCommand):
    help = 'Создать пользователя.'

    def handle(self, *args, **options):
        username = input('Введите имя пользователя: ')
        password = getpass('Введите пароль: ')
        try:
            User.objects.create_user(username, password)
            print('Пользователь успешно создан!')
        except IntegrityError:
            print('Пользователь с такой почтой уже существует.')
        except ValueError:
            print('Переданы неверные значения.')
