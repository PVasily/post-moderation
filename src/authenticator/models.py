import uuid

from django.contrib.auth.base_user import AbstractBaseUser
from django.db import models

from .managers import UserManager


class User(AbstractBaseUser):
    user_id = models.UUIDField(
        'Идентификатор пользователя.',
        primary_key=True, default=uuid.uuid4, editable=False
    )

    username = models.EmailField(
        'Имя пользователя.',
        unique=True, null=False, blank=False
    )

    password = models.CharField(
        'Пароль пользователя.',
        max_length=128, blank=False, null=False
    )

    last_login = models.DateTimeField(
        'Дата-время последней аутентификации пользователя.',
        blank=True, null=True
    )

    created_at = models.DateTimeField(
        'Дата-время создания пользователя.',
        auto_now_add=True, editable=False, null=False
    )

    updated_at = models.DateTimeField(
        'Дата-время последнего обновления пользователя.',
        auto_now=True, null=False
    )

    USERNAME_FIELD = 'user_id'
    REQUIRED_FIELDS = []

    objects = UserManager()

    def __str__(self) -> str:
        return str(self.username)
