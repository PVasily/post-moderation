from typing import Optional
from uuid import UUID

from django.contrib.auth.backends import BaseBackend
from django.contrib.auth.hashers import check_password
from django.http import HttpRequest

from authenticator.models import User


class AuthBackend(BaseBackend):
    def authenticate(self, request: HttpRequest, username: str, password: str) -> Optional[User]:
        try:
            user = User.objects.get(username=username)
            if check_password(password, user.password):
                return user
        except User.DoesNotExist:
            pass
        return None

    def get_user(self, user_id: UUID) -> Optional[User]:
        try:
            return User.objects.get(user_id=user_id)
        except User.DoesNotExist:
            return None
