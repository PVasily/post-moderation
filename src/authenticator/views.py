from django.contrib.auth import authenticate, login, logout
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render, redirect
from django.views import View

from .forms import LoginForm


class LoginView(View):
    form_class = LoginForm
    template_name = 'templates/authenticator/login.html'

    def get(self, request: HttpRequest, *args, **kwargs) -> HttpResponse:
        if request.user.is_authenticated:
            return redirect("/")

        form = self.form_class()

        context = {"form": form}
        return render(request, self.template_name, context)

    def post(self, request: HttpRequest, *args, **kwargs) -> HttpResponse:
        form = self.form_class(request.POST)

        context = {'errors': [], 'form': form}

        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)

                return redirect('/')
        context['errors'].append('FAILED_AUTHENTICATION')
        return render(request, self.template_name, context)


class LogoutView(View):
    def get(self, request: HttpRequest, *args, **kwargs) -> HttpResponse:
        if not request.user.is_authenticated:
            return redirect('login')
        logout(request)
        return redirect('/')
