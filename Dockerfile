FROM python:3.11-slim

ENV VK_APPLICATION_TOKEN 20897dde20897dde20897dde27239e30822208920897dde4559c135b2f198b08d119d57

WORKDIR /app

COPY ./requirements.txt .
RUN pip install -r requirements.txt

COPY . .

RUN python src/manage.py makemigrations
RUN python src/manage.py migrate
